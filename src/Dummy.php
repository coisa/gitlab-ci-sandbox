<?php

namespace App;

/**
 * Class DummyClass
 */
class Dummy
{
    public function __invoke()
    {
        throw new \BadMethodCallException('Oh no! :(');
    }
}
