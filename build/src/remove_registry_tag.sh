#!/usr/bin/env sh

curl \
    --output repos.json \
    --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories"

export CI_REGISTRY_ID=`echo '{"repos":'$(cat repos.json)"}" | jq ".repos[0] .id"`

curl \
    --request DELETE \
    --header "PRIVATE-TOKEN: ${GITLAB_API_TOKEN}" \
    "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/registry/repositories/${CI_REGISTRY_ID}/tags/${CI_BUILD_REF_SLUG}"
