#!/usr/bin/env sh

# Warm up cache layers
( \
    docker pull $RELEASE_IMAGE_NAME || : && \
    docker pull $BUILD_IMAGE_NAME || \
    docker pull composer:${COMPOSER_VERSION:-latest} && \
    docker pull php:${PHP_VERSION:-7.3}-apache \
) >> /dev/null 2>&1

# Build
docker build \
    --cache-from $RELEASE_IMAGE_NAME \
    --cache-from $BUILD_IMAGE_NAME \
    --cache-from composer:${COMPOSER_VERSION:-latest} \
    --cache-from php:${PHP_VERSION:-7.3}-apache \
    --tag ${BUILD_IMAGE_NAME} \
    .

# Push
docker push ${BUILD_IMAGE_NAME}
