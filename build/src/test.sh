#!/usr/bin/env sh

composer install >> /dev/null 2>&1

./vendor/bin/phpunit --coverage-text --colors=never --stop-on-error

# grep -E "[^[:space:]]*Lines:[[:space:]]*[0-9]*\.[0-9]*\%"
# @TODO https://stackoverflow.com/questions/2777579/how-to-output-only-captured-groups-with-sed
