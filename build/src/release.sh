#!/usr/bin/env sh

docker pull "$BUILD_IMAGE_NAME"
docker tag "$BUILD_IMAGE_NAME" "$RELEASE_IMAGE_NAME"
docker push "$RELEASE_IMAGE_NAME"
