Bug Fixes **ONLY**. **NEW FEATURE WILL NOT BE ACCEPTED**! /shrug

# Description

Please include a summary of the change. Please also include relevant motivation and context.

## Previous Behavior

What is the previous behavior? What is the reported "side effect"? It occurs every time or a specific conditions?

## Expected Behavior

Please describe the behavior you are expecting. What this Merge Request claims to fix?

## Related Issues

* Fixes #issue
* Depends on !merge-request

## Impacts Of This Change

Please describe any kind of impact that change could do. This can be positive or negative impact.

**Customer Impact**:
What the user see was changed? Some behavior was visibly changed to the customer? Any kind of business logic was changed and need to be notified to the customer?

**Performance Impact**:
This change affects the running code performance? In a positive or negative way? How did you characterize/test the performance impact?

**Adjacent Services Impact**:
It cause any performance impact in adjacent services? It creates a necessity of specs revalidation or re-implementation? 

## How Has This Been Tested?

Please describe the tests that you ran to verify your changes. Provide instructions so others can reproduce. Please also list any relevant details for your test configuration.

**Steps To Reproduce Tests**:
- [ ] Go to some url
- [ ] Run some script
- [ ] Wait for something
- [ ] Run some SQL on database:

```sql
DROP DATABASE `my_db`; -- /tableflip
```

- [ ] Execute the follow script:

```php
<?php
require_once '/path/to/my/super/magical/solution.php';

echo mySuperMagicFunction();
```

- [ ] Prey so it will work!

**Extra Informations**:
* Operating System: 
* Software Version:
* Absolute URI / Script Name:

## Relevant Logs / Standard Outputs

<details>
<summary>user@servername:/absolute/path/to/file.log</summary>
<pre>
Jul 14 00:08:47 INFO: My custom script info log
Jul 14 00:08:48 ERR: OH NO! Something wrong happens!
</pre>
</details>

## Checklist:

- [ ] My code follows the style guidelines [PSR-1](https://www.php-fig.org/psr/psr-1/) and [PSR-2](https://www.php-fig.org/psr/psr-2/) Coding Standard
- [ ] I have performed a self-review of my own code
- [ ] I have commented my code, particularly in hard-to-understand areas
- [ ] I have made corresponding changes to the documentation
- [ ] My changes generate no new warnings
- [ ] I have added tests that prove my fix is effective or that my feature works
- [ ] New and existing unit tests pass locally with my changes
- [ ] Any dependent changes have been merged and published in downstream modules
- [ ] If you have multiple commits, consider to combine them into a few logically organized commits by [squashing them](https://git-scm.com/book/en/Git-Tools-Rewriting-History#Squashing-Commits)

/copy_metadata #issue
/label ~bug ~critical ~confirmed
/assign @coisa @coisa