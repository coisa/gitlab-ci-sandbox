## Description

Please include a summary of the bug encountered concisely.

## What Is The Current *BUG* Behavior?

What actually happens

## What Is The Expected *Correct* Behavior?

What you should see instead

## Steps To Reproduce

How one can reproduce the issue. Provide instructions so others can reproduce.

- [ ] Go to some url
- [ ] Click some link
- [ ] Submit a number

**Extra Informations**:
* Operating System: 
* Software Version:
* Absolute URI / Script Name:

## Possible Fixes

If you can, link to the line of code that might be responsible for the problem

## Relevant Logs

<details>
<summary>user@servername:/absolute/path/to/file.log</summary>
<pre>
Jul 14 00:08:47 INFO: My custom script info log
Jul 14 00:08:48 ERR: OH NO! Something wrong happens!
</pre>
</details>

## Relevant Screenshots

![Screenshot 01: Image Of A Cat] (http://thecatapi.com/api/images/get?format=src&type=gif)
![Screenshot 02: Now We Have A Dog] (https://api.thedogapi.com/v1/images/search?format=src&mime_types=image/gif)

/label ~bug ~critical
