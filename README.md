[![pipeline status](https://gitlab.com/coisa/gitlab-ci-sandbox/badges/master/pipeline.svg)](https://gitlab.com/coisa/gitlab-ci-sandbox/commits/master)
[![coverage report](https://gitlab.com/coisa/gitlab-ci-sandbox/badges/master/coverage.svg)](https://gitlab.com/coisa/gitlab-ci-sandbox/commits/master)

# CI / CD Variable Dependency
- `GIT_SSH_KEY`: SSH Private Key with read/write access to this repository;
- `GITLAB_API_TOKEN`: GitLab Access Token with read/write access to scope `api`;